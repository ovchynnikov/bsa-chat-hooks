import React  from 'react';
import Header from '../Header/Header';
import MessageInput from '../MessageInput/MessageInput';
import MessageList from '../MessageList/MessageList';
import Preloader from '../Preloader/Preloader';
import Modal from '../Modal/Modal';
import { connect } from 'react-redux';
import * as actions from './actions';


class Chat extends React.Component {
	constructor(props) {
		super(props);

	this.handleInput = this.handleInput.bind(this);
	this.onSendMessage = this.onSendMessage.bind(this);
	this.handleDelete = this.handleDelete.bind(this);
	this.setEditMessage = this.setEditMessage.bind(this);
	this.setLastUserMessage = this.setLastUserMessage.bind(this);
	}

	async componentDidMount() {

    await fetch(`${this.props.url}`)
      .then(response => response.json())
      .then(data => this.props.setMessages(data))
			.catch(err => console.log('fetch error:',err));

			if(this.props.messages.length > 0){
				this.countParticipants();
				this.countMessages();
				this.getLastMessageDate();
				this.setLastUserMessage();
			}
		 }

		 componentDidUpdate(prevProps) {
			 if(this.props.messages !== prevProps.chat.messages) {
			 	 this.countParticipants();
			 	 this.countMessages();
			 	 this.getLastMessageDate();
			 	 this.setLastUserMessage();
			 }
		 }

	countParticipants() {
		const resArr = [];
		const propsMessages = [...this.props.messages]
		propsMessages.forEach(function(item) {
      let i = resArr.findIndex(x => x.user === item.user);
       if(i <= -1) {
         resArr.push({id: item.userId, user: item.user});
       }
  })
		this.props.setChatParticipants(resArr.length)

	}
	
	countMessages() {
		this.props.setMessagesAmount(this.props.messages.length); 
	}

	getLastMessageDate() {
		let lastDate =  new Date(this.props.messages[this.props.messages.length-1].createdAt);
		let dd = lastDate.getDate();
		if (dd < 10) dd = '0' + dd;
	
		let mm = lastDate.getMonth() + 1;
		if (mm < 10) mm = '0' + mm;
	
		let yy = lastDate.getFullYear();
		if (yy < 10) yy = '0' + yy;
	
		let hrs = lastDate.getHours();
		if (hrs < 10) hrs = '0' + hrs;

		let mins = lastDate.getMinutes();
		if (mins < 10) mins = '0' + mins;
		let lastMsgDate = dd + '.' + mm + '.' + yy + ' ' + hrs + ':' + mins;

		this.props.lastMessageDate(lastMsgDate)
	}

	handleInput(e) {
		this.props.handleInput(e.target.value)
	}

	onSendMessage(event) {
		event.preventDefault()
		let { currInputValue } = this.props.chat;
		let { avatar, user, userId } = this.props.currUser;
		if(this.props.chat.inputMode === 'create' && currInputValue.trim().length > 0) {

			let newMessage = {
				avatar: avatar,
				createdAt: new Date().toISOString(),
				editedAt: "",
				id: new Date().toISOString(),
				text: currInputValue.trim(),
				user: user,
				userId: userId
			}

			this.props.createMessage(newMessage);
			this.props.clearCurrInput();
		} 
		else if(this.props.chat.inputMode === 'update' && this.props.chat.modalInput.trim().length > 0) {
			this.onEditItemSubmit(event)
		} 
		
	}
	
	handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      this.onSendMessage(e);
    } if (e.keyCode === 38) {
			const idToEdit = this.props.chat.lastUserMessage.id;
			const currText = this.props.chat.lastUserMessage.text;
			this.props.setModalInput(currText);
			this.props.editMessage(idToEdit);
			this.props.openModal(); 

		}
  }

	handleDelete(event) {
		this.props.deleteMessage(event.target.id);
	}


	onEditItemSubmit = () => {
		const messageId = this.props.chat.editingId;

			let updMessages = this.props.messages.map((item) => item.id === messageId ?
					{...item, text: this.props.chat.modalInput, editedAt: new Date()} : item)
			this.props.saveEditing(updMessages);
    };
  
	onEditModal = event => {
		const text = event.target.value;
		this.props.changeModalInput(text)
	}

	getCurrText = id => {
		let oldMessage = this.props.messages.find(item => item.id === id);
		return oldMessage.text;
	}

	setEditMessage = event => { 
		const idToEdit = event.target.dataset.editid; 
		let currText = this.getCurrText(idToEdit)

		this.props.setModalInput(currText);
		this.props.editMessage(idToEdit);
		this.props.openModal(); 
	}

	setLastUserMessage = () => {
		const lastMessage = {};
		const user = this.props.currUser.user;
		
		this.props.messages.forEach((item) => { if(item.user === user)  lastMessage.last = {...item} });
		this.props.setLastMessage(lastMessage.last);
	}

	render() {
		if(this.props.preloader) {
			return <Preloader />
		}
			  return 	(<div className="chat"> 
							  <Header 
							  chatParticipants={this.props.chat.chatParticipants} 
								countedMessages={this.props.chat.chatMessagesCounted} 
								lastMessageDate={this.props.chat.lastMessageDate}
								/>

              	<MessageList 
								messages={this.props.messages} 
								currUser={this.props.currUser} 
								handleDelete={this.handleDelete} 
								setEditMessage={this.setEditMessage}
								/>

							  <MessageInput 
							  onChange={this.handleInput} 
							  onSubmitHandler={this.onSendMessage} 
							  value={this.props.chat.currInputValue} 
							  mode={this.props.chat.inputMode}
							  onKeyDown={this.handleKeyDown}
							  />
								<Modal 
								message={this.props.chat.lastUserMessage} 
								onClose={this.props.closeModal} 
								showModal={this.props.editModal} 
								modalInput={this.props.chat.modalInput} 
								handleModalInput={this.onEditModal} 
								onSave={this.onSendMessage}
								/>
						</div>)
	}
}

const mapStateToProps = (state) => {
	return {
		chat: state.chat,
		currUser: state.chat.currUser,
		messages: state.chat.messages,
		lastUserMessage: state.chat.lastUserMessage,
		preloader: state.chat.preloader,
		editModal: state.chat.editModal,

	}
}

const mapDispatchToProps = {
	...actions
	 
}
export default connect(mapStateToProps, mapDispatchToProps)(Chat);