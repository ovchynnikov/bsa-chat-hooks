import {
    CREATE_MESSAGE,
    EDIT_MESSAGE,
    DELETE_MESSAGE,
    SET_CHAT_PARTICIPANTS,
    SET_LAST_MESSAGE_DATE,
    SET_COUNT_CHAT_MESSAGES,
    SET_MESSAGES,
    SET_CURR_INPUT,
    CLEAR_INPUT,
    SET_LAST_MESSAGE,
    CLOSE_MODAL,
    CHANGE_MODAL_INPUT,
    OPEN_MODAL,
    SET_MODAL_INPUT,
    SAVE_EDITING
} from './actionTypes';

export const setMessages = data => ({
    type: SET_MESSAGES,
    payload: {
        data
    }
})

export const setLastMessage = data => ({
    type: SET_LAST_MESSAGE,
    payload: {
        data
    }
})

export const setModalInput = data => ({
    type: SET_MODAL_INPUT,
    payload: {
        data
    }
})

export const changeModalInput = data => ({
    type: CHANGE_MODAL_INPUT,
    payload: {
        data
    }
})

export const closeModal = () => ({
    type: CLOSE_MODAL
})

export const openModal = () => ({
    type: OPEN_MODAL
})

export const createMessage = newMessage => ({
    type: CREATE_MESSAGE,
    payload: {
        newMessage
    }
})

export const editMessage = (id, data) => ({
    type: EDIT_MESSAGE,
    payload: {
        id,
        data
    }
})

export const saveEditing = data => ({
    type: SAVE_EDITING,
    payload: {
        data
    }
})

export const clearCurrInput = () => ({
    type: CLEAR_INPUT
})

export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
})

export const setChatParticipants = data => ({
    type: SET_CHAT_PARTICIPANTS,
    payload: {
        data
    }
})

export const lastMessageDate = data => ({
    type: SET_LAST_MESSAGE_DATE,
    payload: {
        data
    }
})

export const setMessagesAmount = data => ({
    type: SET_COUNT_CHAT_MESSAGES,
    payload: {
        data
    }
})

export const handleInput = text => ({
    type: SET_CURR_INPUT,
    payload: {
        text
    }
})