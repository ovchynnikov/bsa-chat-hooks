import {
    CREATE_MESSAGE,
    EDIT_MESSAGE,
    DELETE_MESSAGE,
    SET_CHAT_PARTICIPANTS,
    SET_LAST_MESSAGE_DATE,
    SET_MESSAGES,
    SET_COUNT_CHAT_MESSAGES,
    SET_CURR_INPUT,
    CLEAR_INPUT,
    SET_LAST_MESSAGE,
    CLOSE_MODAL,
    CHANGE_MODAL_INPUT,
    SET_MODAL_INPUT,
    OPEN_MODAL,
    SAVE_EDITING
} from './actionTypes';

const initialState = {

    messages: [],
    lastMessageDate: null,
    chatParticipants: 0,
    chatMessagesCounted: 0,
    currUser: {
        avatar: "https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0MzYxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ",
        user: "Helen",
        userId: "4b003c20-1b8f-11e8-9629-c7eca82aa7bd"
    },
    inputMode: 'create',
    editingId: null,
    currInputValue: '',
    modalInput: '',
    lastUserMessage: {},
    editModal: false,
    preloader: true,
}

// eslint-disable-next-line
export default function(state = initialState, action) {
    switch (action.type) {

        case SET_MESSAGES:
            {

                return {
                    ...state,
                    messages: action.payload.data,
                    preloader: false
                }
            }

        case CREATE_MESSAGE:
            {
                const newMessage = action.payload.newMessage;
                const updMessages = [...state.messages, newMessage];
                return {
                    ...state,
                    messages: updMessages
                }
            }

        case EDIT_MESSAGE:
            {
                const { id, currInputValue } = action.payload;

                return {
                    ...state,
                    editingId: id,
                    inputMode: 'update',
                    currInputValue: currInputValue,
                }
            }

        case SAVE_EDITING:
            {
                const updMessages = action.payload.data;

                return {
                    ...state,
                    messages: updMessages,
                    editModal: false,
                    modalInput: '',
                    editingId: '',
                    inputMode: 'create'
                }

            }

        case DELETE_MESSAGE:
            {
                const id = action.payload.id;
                const updMessages = state.messages.filter(message => message.id !== id);

                return {
                    ...state,
                    messages: updMessages
                }
            }

        case CLEAR_INPUT:
            {

                return {
                    ...state,
                    currInputValue: ''
                }
            }

        case CHANGE_MODAL_INPUT:
            {

                return {
                    ...state,
                    modalInput: action.payload.data
                }
            }
        case SET_MODAL_INPUT:
            {

                return {
                    ...state,
                    modalInput: action.payload.data
                }
            }

        case CLOSE_MODAL:
            {

                return {
                    ...state,
                    editingId: '',
                    editModal: false,
                    inputMode: 'create'
                }
            }

        case OPEN_MODAL:
            {

                return {
                    ...state,
                    editModal: true
                }
            }

        case SET_CHAT_PARTICIPANTS:
            {

                return {
                    ...state,
                    chatParticipants: action.payload.data
                }
            }

        case SET_LAST_MESSAGE_DATE:
            {
                const lastMessageDate = action.payload.data;

                return {
                    ...state,
                    lastMessageDate
                }
            }

        case SET_COUNT_CHAT_MESSAGES:
            {
                const messagesLength = action.payload.data;

                return {
                    ...state,
                    chatMessagesCounted: messagesLength
                }
            }

        case SET_CURR_INPUT:
            {
                const text = action.payload.text;

                return {
                    ...state,
                    currInputValue: text
                }
            }

        case SET_LAST_MESSAGE:
            {
                return {
                    ...state,
                    lastUserMessage: action.payload.data,
                }
            }
        default:
            return state;
    }
}