import React from 'react';
import './Divider.css';

function Divider(props) {
        
            return (
                <div className="messages-divider">{props.dateToShow}<hr /></div>
            )
}

export default Divider;