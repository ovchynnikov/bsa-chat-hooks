import React from 'react';
import './Modal.css';

const Modal = props => {
            return (
                <>
                  <div className={props.showModal ? `edit-message-modal modal-shown` :  `edit-message-modal hide-modal`}>
                      <div className="modal-body">
                          <h3>Edit the message</h3>
                          <textarea className="edit-message-input" value={props.modalInput} onChange={props.handleModalInput}></textarea>
                          <button className="modalButton edit-message-close" onClick={props.onClose}>Cancel</button>
                          <button className="modalButton edit-message-button" onClick={props.onSave}>Save</button>
                      </div>
                  </div>
                </>
            )
    }
export default Modal;